package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class LegendaryItemTest {
    @Test
    public void changeOfQualityInOneDay() throws QualityException {
        LegendaryItem item = new LegendaryItem("Some item name",10,10);
        item.passageOfTimeAndQualityChange();
        assert item.getQuality() == 80;
    }
    @Test
    public void changeOfQualityUntilLastDay() throws QualityException {
        LegendaryItem item = new LegendaryItem("Some item name",10,10);
        item.passageOfTimeAndQualityChange();
        item.passageOfTimeAndQualityChange();
        assert item.getQuality() == 80;
    }
    @Test
    public void changeOfQualityAfterLastDay() throws QualityException {
        LegendaryItem item = new LegendaryItem("Some item name",10,1);
        item.passageOfTimeAndQualityChange();
        item.passageOfTimeAndQualityChange();
        assert item.getQuality() == 80;
    }


}
