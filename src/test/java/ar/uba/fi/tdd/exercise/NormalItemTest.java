package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class NormalItemTest {
    @Test
    public void changeOfQualityInOneDay() throws QualityException {
        NormalItem item = new NormalItem("Some item name",10,10,10);
        item.passageOfTimeAndQualityChange();
        assert item.getQuality() == 9;
    }
    @Test
    public void changeOfQualityUntilLastDay() throws QualityException {
        NormalItem item = new NormalItem("Some item name",10,10,2);
        item.passageOfTimeAndQualityChange();
        item.passageOfTimeAndQualityChange();
        assert item.getQuality() == 7;
    }
    @Test
    public void changeOfQualityAfterLastDay() throws QualityException {
        NormalItem item = new NormalItem("Some item name",10,10,1);
        item.passageOfTimeAndQualityChange();
        item.passageOfTimeAndQualityChange();
        assert item.getQuality() == 6;
    }

    @Test
    public void incorrectQualityValue() {
        try {
            new NormalItem("Some item name", 10, 51, 1);
        }
        catch (QualityException e){
            assert Boolean.TRUE;
            return;
        }
        assert Boolean.FALSE;
    }


}
