package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class BrieItemTest {
    @Test
    public void changeOfQualityInOneDay() throws QualityException {
        Brie item = new Brie("Some item name",10,10,10);
        item.passageOfTimeAndQualityChange();
        assert item.getQuality() == 11;
    }
    @Test
    public void changeOfQualityUntilLastDay() throws QualityException {
        Brie item = new Brie("Some item name",10,10,2);
        item.passageOfTimeAndQualityChange();
        assert item.getQuality() == 11;
    }
    @Test
    public void changeOfQualityAfterLastDay() throws QualityException {
        Brie item = new Brie("Some item name",10,10,1);
        item.passageOfTimeAndQualityChange();
        item.passageOfTimeAndQualityChange();
        assert item.getQuality() == 14;
    }

    @Test
    public void incorrectQualityValue() {
        try {
            new Brie("Some item name", 10, 51, 1);
        }
        catch (QualityException e){
            assert Boolean.TRUE;
            return;
        }
        assert Boolean.FALSE;
    }


}
