package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;


@SpringBootTest
class GildedRoseTest {

	@Test
	public void emptyListOfItems() {
		List<Item> list = new ArrayList<>();
		GildedRose gildedRose = new GildedRose(list);
		try {
			gildedRose.updateQuality();
		}
		catch (EmptyListItemException e){
			assert Boolean.TRUE;
			return;
		}
		assert Boolean.FALSE;
	}

	@Test
	public void getQualityOfOnlyNormalItemsAfterOneDay() throws QualityException {
		List<Item> list = new ArrayList<>();
		NormalItem item_1 = new NormalItem("Item1",10,10,10);
		NormalItem item_2 = new NormalItem("Item1",10,10,11);
		list.add(item_1);
		list.add(item_2);
		GildedRose gildedRose = new GildedRose(list);
		try {
			gildedRose.updateQuality();
		}
		catch (EmptyListItemException e){
			assert Boolean.FALSE;
		}
		assert item_1.getQuality() == 9;
		assert item_2.getQuality() == 9;

	}

	@Test
	public void getQualityOfOnlyBrieAfterOneDay() throws QualityException {
		List<Item> list = new ArrayList<>();
		Brie item_1 = new Brie("Item1",10,10,1);
		Brie item_2 = new Brie("Item1",10,10,1);
		list.add(item_1);
		list.add(item_2);
		GildedRose gildedRose = new GildedRose(list);
		try {
			gildedRose.updateQuality();
		}
		catch (EmptyListItemException e){
			assert Boolean.FALSE;
		}
		assert item_1.getQuality() == 12;
		assert item_2.getQuality() == 12;

	}

	@Test
	public void getQualityOfOnlyBackStageAfterOneDay() throws QualityException {
		List<Item> list = new ArrayList<>();
		BackStage item_1 = new BackStage("Item1",10,10,1);
		BackStage item_2 = new BackStage("Item2",10,10,1);
		list.add(item_1);
		list.add(item_2);
		GildedRose gildedRose = new GildedRose(list);
		try {
			gildedRose.updateQuality();
		}
		catch (EmptyListItemException e){
			assert Boolean.FALSE;
		}
		assert item_1.getQuality() == 13;
		assert item_2.getQuality() == 13;

	}

	@Test
	public void getQualityOfOnlyLegendaryAfterOneDay() throws QualityException {
		List<Item> list = new ArrayList<>();
		LegendaryItem item_1 = new LegendaryItem("Wow",10,10);
		LegendaryItem item_2 = new LegendaryItem("WoW2",10,10);
		list.add(item_1);
		list.add(item_2);
		GildedRose gildedRose = new GildedRose(list);
		try {
			gildedRose.updateQuality();
		}
		catch (EmptyListItemException e){
			assert Boolean.FALSE;
		}
		assert item_1.getQuality() == 80;
		assert item_2.getQuality() == 80;

	}

	@Test
	public void getQualityOfMixItemsAfterOneDay() throws QualityException {
		List<Item> list = new ArrayList<>();
		LegendaryItem legendaryItem = new LegendaryItem("Wow",10,10);
		BackStage backStage = new BackStage("Item2",10,10,1);
		Brie brie = new Brie("Item1",10,10,1);
		NormalItem normalItem = new NormalItem("Item1",10,10,11);
		list.add(legendaryItem);
		list.add(backStage);
		list.add(brie);
		list.add(normalItem);
		GildedRose gildedRose = new GildedRose(list);
		try {
			gildedRose.updateQuality();
		}
		catch (EmptyListItemException e){
			assert Boolean.FALSE;
		}
		assert legendaryItem.getQuality() == 80;
		assert backStage.getQuality() == 13;
		assert brie.getQuality() == 12;
		assert normalItem.getQuality() == 9;

	}

}
