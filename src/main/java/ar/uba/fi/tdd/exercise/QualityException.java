package ar.uba.fi.tdd.exercise;

public class QualityException extends Throwable {
    public QualityException(String msg) {
        super(msg);
    }
}
