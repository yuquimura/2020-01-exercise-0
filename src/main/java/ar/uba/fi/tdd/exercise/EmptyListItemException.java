package ar.uba.fi.tdd.exercise;

public class EmptyListItemException extends Throwable {
    public EmptyListItemException(String msg) {
        super(msg);
    }
}
