package ar.uba.fi.tdd.exercise;

public class NormalItem extends Item {

    public NormalItem(String name, int sellIn, int quality, int daysRemaining) throws QualityException {
        super(name, sellIn, quality, daysRemaining);
        if (quality >50){
            throw new QualityException("The quality value is higher than 50");
        }
    }

    @Override
    protected void passageOfTimeAndQualityChange() {
        daysRemaining--;

        if (quality == 0) {
            return;
        }

        quality--;

        if (daysRemaining <= 0) {
            quality--;
        }
    }
}
