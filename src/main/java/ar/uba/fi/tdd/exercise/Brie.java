package ar.uba.fi.tdd.exercise;

public class Brie extends Item{
    public Brie(String name, int sellIn, int quality, int daysRemaining) throws QualityException {
        super(name, sellIn, quality, daysRemaining);
        if (quality >50){
            throw new QualityException("The quality value is higher than 50");
        }
    }

    @Override
    protected void passageOfTimeAndQualityChange() {
        daysRemaining--;

        if (quality >= 50) {
            return;
        }

        quality++;

        if (daysRemaining <= 0) {
            quality++;
        }
    }}
