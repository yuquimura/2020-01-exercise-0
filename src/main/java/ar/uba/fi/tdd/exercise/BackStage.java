package ar.uba.fi.tdd.exercise;

class BackStage extends Item {
    public BackStage(String name, int sellIn, int quality, int daysRemaining) throws QualityException {
        super(name, sellIn, quality, daysRemaining);
        if (quality >50){
            throw new QualityException("The quality value is higher than 50");
        }
    }

    @Override
    protected void passageOfTimeAndQualityChange() {
        daysRemaining--;

        if (quality >= 50) {
            return;
        }

        if (daysRemaining < 0) {
            quality = 0;
            return;
        }

        quality++;

        if (daysRemaining < 10) {
            quality++;
        }

        if (daysRemaining < 5) {
            quality++;
        }
    }
}