package ar.uba.fi.tdd.exercise;


public class LegendaryItem extends Item{

    private static final int QUALITY = 80;
    public LegendaryItem(String name, int sellIn, int daysRemaining) throws QualityException {
        super(name, sellIn, QUALITY, daysRemaining);
    }
    @Override
    protected void passageOfTimeAndQualityChange() {
        daysRemaining--;
    }


}
