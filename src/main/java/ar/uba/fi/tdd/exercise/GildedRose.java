
package ar.uba.fi.tdd.exercise;

import java.util.List;

class GildedRose {
  private final List<Item> items;

    public GildedRose(final List<Item> list) {
        items = list;
    }

    public void updateQuality() throws EmptyListItemException {
        if (items.isEmpty()) {
            throw new EmptyListItemException("Empty list");
        }
        for (Item item : items) {
            item.passageOfTimeAndQualityChange();
        }
    }
}
