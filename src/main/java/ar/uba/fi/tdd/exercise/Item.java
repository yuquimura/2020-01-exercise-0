package ar.uba.fi.tdd.exercise;

public class Item {

    protected String name;

    protected int sellIn;

    protected int quality;

    protected int daysRemaining;

    public Item(final String name, int sellIn, int quality, int daysRemaining) throws QualityException {
        this.name = name;
        this.sellIn = sellIn;
        this.quality = quality;
        this.daysRemaining = daysRemaining;
    }

    // shows the Item representation
   @Override
   public String toString() {
     return this.name + ", " + this.sellIn + ", " + this.quality;
    }

    public String getName(){
        return this.name;
    }

    public int getSellIn(){
        return this.sellIn;
    }

    public int getQuality() {
        return quality;
    }

    protected void passageOfTimeAndQualityChange() {
    }

 }
